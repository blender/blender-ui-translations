<!--
Keep this document short & concise,
linking to external resources instead of including content in-line.
-->

Blender UI Translation
======================

This repository contains the raw Blender UI translation files, using the GNUGettext format. Besides administration updates, no direct commits are expected to happen on this repository. Actual translation effort happens on [Blender Translate](https://translate.blender.org/projects/blender-ui/).

The translations used by Blender builds are a compacted and cleaned-up version of these files, stored directly in the [main Blender repository](https://projects.blender.org/blender/blender/src/branch/main/locale).

Project Pages
-------------

- [Blender Website](http://www.blender.org)
- [How To Translate Blender](https://wiki.blender.org/wiki/Process/Translate_Blender)

Development
-----------

- [Handling i18N in Blender code](https://wiki.blender.org/wiki/Source/Interface/Internationalization)

License
-------

The files in this repository are distributed under the same license as the Blender package.
