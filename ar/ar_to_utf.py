#coding: utf-8

# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
#    Authers:
#      -Yousef Harfoush
#      -Dalai Felinto
#      -Bastien Montagne
#
#    based on The Arabic Writer script by Omar Muhammad
#    thanks for Rabeh Torchi for ideas
#
# ***** END GPL LICENSE BLOCK *****

import sys
import os
import re

# Isolated, Beginning, Middle, End
# http://en.wikipedia.org/wiki/Arabic_alphabet
# http://en.wikipedia.org/wiki/Arabic_characters_in_Unicode
alfmd = ["ﺁ","ﺁ","ﺂ","ﺂ"]
alfhz = ["ﺃ","ﺃ","ﺄ","ﺄ"]
wowhz = ["ﺅ","ﺅ","ﺆ","ﺆ"]
alfxr = ["ﺇ","ﺇ","ﺈ","ﺈ"]
hamzk = ["ﺉ","ﺋ","ﺌ","ﺊ"]
alfff = ["ﺍ","ﺍ","ﺎ","ﺎ"]
baaaa = ["ﺏ","ﺑ","ﺒ","ﺐ"]
tamrb = ["ﺓ","ﺓ","ﺔ","ﺔ"]
taaaa = ["ﺕ","ﺗ","ﺘ","ﺖ"]
thaaa = ["ﺙ","ﺛ","ﺜ","ﺚ"]
geeem = ["ﺝ","ﺟ","ﺠ","ﺞ"]
haaaa = ["ﺡ","ﺣ","ﺤ","ﺢ"]
khaaa = ["ﺥ","ﺧ","ﺨ","ﺦ"]
daaal = ["ﺩ","ﺩ","ﺪ","ﺪ"]
thaal = ["ﺫ","ﺫ","ﺬ","ﺬ"]
raaaa = ["ﺭ","ﺭ","ﺮ","ﺮ"]
zaaai = ["ﺯ","ﺯ","ﺰ","ﺰ"]
seeen = ["ﺱ","ﺳ","ﺴ","ﺲ"]
sheen = ["ﺵ","ﺷ","ﺸ","ﺶ"]
saaad = ["ﺹ","ﺻ","ﺼ","ﺺ"]
daaad = ["ﺽ","ﺿ","ﻀ","ﺾ"]
taaah = ["ﻁ","ﻃ","ﻄ","ﻂ"]
daaah = ["ﻅ","ﻇ","ﻈ","ﻆ"]
aayen = ["ﻉ","ﻋ","ﻌ","ﻊ"]
gayen = ["ﻍ","ﻏ","ﻐ","ﻎ"]
faaaa = ["ﻑ","ﻓ","ﻔ","ﻒ"]
qaaaf = ["ﻕ","ﻗ","ﻘ","ﻖ"]
kaaaf = ["ﻙ","ﻛ","ﻜ","ﻚ"]
laaam = ["ﻝ","ﻟ","ﻠ","ﻞ"]
meeem = ["ﻡ","ﻣ","ﻤ","ﻢ"]
nooon = ["ﻥ","ﻧ","ﻨ","ﻦ"]
hhhhh = ["ﻩ","ﻫ","ﻬ","ﻪ"]
wowww = ["ﻭ","ﻭ","ﻮ","ﻮ"]
yaamd = ["ﻯ","ﻯ","ﻰ","ﻰ"]
yaaaa = ["ﻱ","ﻳ","ﻴ","ﻲ"]
laamd = ["ﻵ","ﻵ","ﻶ","ﻶ"]
laahz = ["ﻷ","ﻷ","ﻸ","ﻸ"]
laaxr = ["ﻹ","ﻹ","ﻺ","ﻺ"]
laaaa = ["ﻻ","ﻻ","ﻼ","ﻼ"]

# these are not used for now but maybe later---------
# defining numbers
numbers ="0123456789٠١٢٣٤٥٦٧٨٩"

# defining arabic unicodec chars
unicodec ="ﺁﺁﺂﺂﺃﺃﺄﺄﺅﺅﺆﺆﺇﺇﺈﺈﺉﺋﺌﺊﺍﺍﺎﺎﺏﺑﺒﺐﺓﺓﺔﺔﺕﺗﺘﺖﺙﺛﺜﺚﺝﺟﺠﺞﺡﺣﺤﺢﺥﺧﺨﺦﺩﺩﺪﺪﺫﺫﺬﺬﺭﺭﺮﺮﺯﺯﺰﺰﺱﺳﺴﺲﺵﺷﺸﺶﺹﺻﺼﺺﺽﺿﻀﺾﻁﻃﻄﻂﻅﻇﻈﻆﻉﻋﻌﻊﻍﻏﻐﻎﻑﻓﻔﻒﻕﻗﻘﻖﻙﻛﻜﻚﻝﻟﻠﻞﻡﻣﻤﻢﻥﻧﻨﻦﻩﻫﻬﻪﻭﻭﻮﻮﻯﻯﻰﻰﻱﻳﻴﻲﻵﻵﻶﻶﻷﻷﻸﻸﻹﻹﻺﻺﻻﻻﻼﻼ"

# defining the harakat
harakat ="ًٌٍَُِّْْ"

# defining other symbols
sym ="ًٌٍَُِّـ.،؟ @#$%^&*-+|\/=~(){}ْ,:"
#---------------------------------------------------

# letters that have only Isolated and End forms
# (and work as word breakers) + deriveds of lam
wordbreak ="آأؤإاةدذرزوﻵﻷﻹﻻ"

# defining all arabic letters + harakat
arabic ="ًٌٍَُِّْْئءؤرلاىةوزظشسيبلاتنمكطضصثقفغعهخحجدذْلآآلأأـلإإ،؟"

def ProcessInput(charPre, charOp, charPost):
    #process each letter, submit it to tests return

    chr = ""
    
    # get the position
    if charPre == "Null" and charPost != "Null": # WILL BE ONLY POS 0 OR 1
        if charOp in wordbreak:#  WILL BE ONLY POS 0
            pos = 0 # isolated
        else: #  WILL BE ONLY POS 1
            pos = 1 # start
    elif charPost == "Null" and charPre != "Null": # WILL BE ONLY POS 0 OR 3
        if charOp in wordbreak:#  WILL BE ONLY POS 0
            pos = 0 # isolated
        else: #  WILL BE ONLY POS 3
            pos = 3 # end
    elif charPost == "Null" and charPre == "Null": # WILL BE ONLY POS 0
        pos = 0 # isolated
    else: # WILL BE ONLY POS 1 OR 2
        if charPre in wordbreak:#  WILL BE ONLY POS 0
            pos = 1 # start
        else: #  WILL BE ONLY POS 2
            pos = 2 # middle

    # find what char to aggregate to the phrase based on the input and its
    # position in the word.    
    if charOp=="ء": chr="ﺀ"
    elif charOp=="آ":#skip this for it will be compined by the laam next
        if not charPre=="ل":
            chr=alfmd[pos]
    elif charOp=="أ": 
        if not charPre=="ل":
            chr=alfhz[pos]
    elif charOp=="إ": 
        if not charPre=="ل":
            chr=alfxr[pos]
    elif charOp=="ا": 
        if not charPre=="ل":
            chr=alfff[pos]
    elif charOp=="ؤ": chr=wowhz[pos]
    elif charOp=="ئ": chr=hamzk[pos]
    elif charOp=="ب": chr=baaaa[pos]
    elif charOp=="ة": chr=tamrb[pos]
    elif charOp=="ت": chr=taaaa[pos]
    elif charOp=="ث": chr=thaaa[pos]
    elif charOp=="ج": chr=geeem[pos]
    elif charOp=="ح": chr=haaaa[pos]
    elif charOp=="خ": chr=khaaa[pos]
    elif charOp=="د": chr=daaal[pos]
    elif charOp=="ذ": chr=thaal[pos]
    elif charOp=="ر": chr=raaaa[pos]
    elif charOp=="ز": chr=zaaai[pos]
    elif charOp=="س": chr=seeen[pos]
    elif charOp=="ش": chr=sheen[pos]
    elif charOp=="ص": chr=saaad[pos]
    elif charOp=="ض": chr=daaad[pos]
    elif charOp=="ط": chr=taaah[pos]
    elif charOp=="ظ": chr=daaah[pos]
    elif charOp=="ع": chr=aayen[pos]
    elif charOp=="غ": chr=gayen[pos]
    elif charOp=="ف": chr=faaaa[pos]
    elif charOp=="ق": chr=qaaaf[pos]
    elif charOp=="ك": chr=kaaaf[pos]
    elif charOp=="ل":
    # dealing with (la combination)
    # in this case the char has two chars in one
        if charPost == " ": chr=laaam[pos]
        elif charPost=="ا": chr=laaaa[pos]
        elif charPost=="أ": chr=laahz[pos]
        elif charPost=="إ": chr=laaxr[pos]
        elif charPost=="آ": chr=laamd[pos]
        else: chr=laaam[pos]
    elif charOp=="م": chr=meeem[pos]
    elif charOp=="ن": chr=nooon[pos]
    elif charOp=="ه": chr=hhhhh[pos]
    elif charOp=="و": chr=wowww[pos]
    elif charOp=="ى": chr=yaamd[pos]
    elif charOp=="ي": chr=yaaaa[pos]
    elif charOp=="لآ": chr=laamd[pos]
    elif charOp=="لأ": chr=laahz[pos]
    elif charOp=="لإ": chr=laaxr[pos]
    elif charOp=="لا": chr=laaaa[pos]
    else:
        chr = charOp
    return chr

# check if the line needs editing
def needsEditing(line, x):
    if line.startswith('msgstr "'):
        if not line.startswith('msgstr ""'):
            return 'true'
    elif line.startswith('"'):
        words=line[1:-1].split(' ')
        for word in words:
            chars = list(word)
            if chars and chars[0] in arabic:
                return 'true'

def Start(fileR, fileW):
    #Open the .po file and do a special reverse in the msgstr lines
    fileR = open(fileR, "r",-1, "utf-8")
    fileW = open(fileW, "w",-1, "utf-8")
    x = 0
    for line in fileR:# loop over all lines
        #x += 1
        #print(x)
        #if x == 148:
        #    print('')
        if needsEditing(line, x) == 'true':
            strng = line.rstrip("\n\r")
            if line.startswith('"'):#supporting multiline but fully as the fmtmsg 
                words = strng[1:-1].split(' ')
                fileW.write('"')
            else:
                words = strng[8:-1].split(' ')
                fileW.write('msgstr "')
            j = 0 # loop for getting the Arabic word position in words array
            for word in words:
                chars = list(word)
                lenchars = len(chars)
                if len(chars) > 0 and chars[0] in arabic:
                    i = 0 # loop for getting the next char
                    accomulateChar = ''
                    #print(word)
                    for chrOb in chars:# loop over chars in an Arabic word
                        if lenchars == 1: # isolated char in a word
                            rslt = ProcessInput("Null", chrOb, "Null")
                        elif i == 0: # 1st char in a word
                            rslt = ProcessInput("Null", chrOb, chars[i+1])
                        elif i == lenchars-1: # last char in a word
                            rslt = ProcessInput(chars[i-1], chrOb, "Null")
                        #elif chrOb == "": # last char in a word

                        else:
                            rslt = ProcessInput(chars[i-1], chrOb, chars[i+1])
                        i+=1
                        accomulateChar += rslt # recompose the word
                    #replace with it's reverse
                    words[j] = accomulateChar[::-1]
                j+=1
            for word in reversed(words):
                fileW.write(word + ' ')
            fileW.write('"\n')
            j=0
        else:
            fileW.write(line)
    fileR.close()
    fileW.close()

if __name__ == "__main__":
    #argument parsing
    import argparse
    parser = argparse.ArgumentParser(description="Open the .po file and do " \
                                                 "a special reverse in the " \
                                                 "msgstr lines.")
    parser.add_argument('input', help="Input .po file.")
    parser.add_argument('output', default=None, help="Output .po file.")
    args = parser.parse_args()

    file_input = os.path.abspath(args.input)
    if not file_input.endswith(".po"):
        print("Error: Wrong file format. Use: `python3 ar_to_utf.py ar.po [ar_done.po]`")
        exit()

    # the last parameter is optional
    if args.output:
        file_output = os.path.abspath(args.output)
    else:
        file_output = "{}_done.po".format(file_input[:-3])

    # parse the file
    Start(file_input, file_output)
